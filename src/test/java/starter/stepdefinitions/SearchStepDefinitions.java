package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import org.hamcrest.Matchers;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {
    Actor he;
    String basePath;
    SoftAssertions softAssertions = new SoftAssertions();
    @Given("the base URL is {string}")
    public void theBaseURLIs(String basePath) {
        this.basePath=basePath;
    }

    @When("{string} calls endpoint {string}")
    public void callsEndpoint(String actorName, String product) {
        he=Actor.named(actorName);
        he.whoCan(CallAnApi.at(basePath));
        he.attemptsTo(Get.resource(product));

    }
    @Then("{string} sees the {string}")
    public void seesThe(String arg0, String product) {

        he.should(seeThatResponse("Every item in the body contains desired product ",response->response.body("title",everyItem(containsStringIgnoringCase(product)))));
        softAssertions.assertAll();
    }


    @Then("{string} see result detail {string}")
    public void seeResultDetail(String arg0, String result) {
        he.should(seeThatResponse("Response not null",response->response.body(notNullValue())));
        he.should(seeThatResponse("Not found result",response -> response.body("detail.error", is(Boolean.parseBoolean(result)))));
    }
}
