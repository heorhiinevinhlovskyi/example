## Report  

### Introduction  
Dear Hiring team,   
Thank you very much for your time and attention.  
I appreciate this opportunity to highlight my knowledge and skills to you. 

#### Project changes that were applied  

To carry out successful tests of the current project, I used the following tools:  
Maven  
Serenity (Screenplay test design pattern)  
Cucumber (BDD implementation)  
AssertJ   
Hamcrest Matchers   
JUnit 4  
Gherkin language  

In order to facilitate the modification of data or environment from the .feature file, I rewrote the scenarios.  
The dependencies in the pom.xml file were updated and adjusted accordingly.  
The serenity.conf file was adjusted for better reporting purposes. 
Additionally, a .gitlab-ci.yml file was created for the CI pipeline in GitLab, allowing the ability to download the report results.  

#### Bugs were found  
Before conducting the tests, the given API URL was manually checked with Postman.  

An unexpected bug was discovered:  
During the first scenario where a person searches for "orange", they do not receive a list of items containing only the word "orange".  
There are additional items that do not contain the word "orange" in their links, images, or titles.  

### How to run tests from GitLab
Go to the repository's pipeline link: https://gitlab.com/heorhiinevinhlovskyi/example/-/pipelines  
Click the "Run Pipeline" blue button and then one more time.  
After the run is executed, please download the report by clicking the artifacts download button.   
Unzip the downloaded file and navigate to the following path: artifacts/serenity_report/target/site/serenity/index.html.   
Open index.html with a browser to view the Serenity report. 

### How to run tests on your local machine
Download the example project repository from the email and unzip it.
Open your preferred IDE and open the repository folder as a new project.
Open the console and execute the command: mvn clean verify.  

### How to write tests in the present framework
Open the new.feature file in the .resources/features folder and write test scenarios based on the acceptance criteria of your user story  
using the Gherkin language.   
Create separate classes for the methods you plan to apply in order to centralize and make them more reusable.   
Create a new test class in the starter/step-definition folder and implement the steps of your feature file using the annotations @Given, @When, @Then.

